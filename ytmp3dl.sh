#!/bin/bash
#
# Script for extracting sound (MP3) from YouTube videos (URLs).
#
# Passes arguments to youtube-dl that does all the work. :p
#
# Prerequisites example in Ubuntu 16.04 or Debian 8:
#
# $ sudo apt-get install python3 python3-venv git ffmpeg
# $ git clone https://bitbucket.org/jhh/ytmp3dl.git $HOME/git/ytmp3dl
# $ $HOME/git/ytmp3dl/ytmp3dl.sh
#
# You might want to add ytmp3dl.sh to $PATH, for example.
#
# $ echo "export PATH=\"\$HOME/git/ytmp3dl:\$PATH\"" >> $HOME/.bashrc
# $ exec bash
# $ ytmp3dl.sh
#

# Exit immiediately if command exits with a non-zero status.
set -e

# Directory for youtube-dl and Python stuff.
UTILS_DIR=$(dirname $(mktemp -u))/${USER}_ytmp3dl

# Target download folder.
BASE_DOWNLOAD_DIR=.

SCRIPT_NAME=ytmp3dl.sh

if [ $# -eq 0 ]; then
    echo "Welcome to the YouTube MP3 download script."
    echo
    echo "Usage: $SCRIPT_NAME \"URL\" [\"URL\"...]"
    exit 0
fi

if ! mkdir -p "$UTILS_DIR"; then
    echo "$SCRIPT_NAME: Couldn't create \$UTILS_DIR: ${UTILS_DIR} ..."
    exit 1
fi

for var in "$@"; do
    match="$(echo "$var" | grep -oP 'http[s]?:\/\/(www\.youtube\.com\/watch\?v=|youtu.be\/)[\w\-]{11}')"
    if [ -z $match ]; then
        echo "$SCRIPT_NAME: The URL \"$var\" doesn't look a YouTube video address."
        echo "$SCRIPT_NAME: Expected \"https://www.youtube.com/watch?v=xxxxxxxxxxx\", or \"https://youtu.be/xxxxxxxxxxx\" ..."
        echo "$SCRIPT_NAME: Please try again."
        exit 3
    fi
done

if [ ! -d "venv" ]; then
    /usr/bin/env python3 -m venv "${UTILS_DIR}/venv"
fi

source "${UTILS_DIR}/venv/bin/activate"

if [ -z "$VIRTUAL_ENV" ]; then
    echo "$SCRIPT_NAME: No \$VIRTUAL_ENV: $VIRTUAL_ENV ..."
    exit 4
fi

# Install youtube-dl if not available.
if ! hash youtube-dl 2>/dev/null; then
    pip install --upgrade pip youtube_dl
fi

for var in "$@"; do
    # Save tracks in "playlist folder" if URL have "list" argument in it,
    # otherwise save the youtube mp3 in current folder.
    if [ "$(echo "$var" | grep -oP '[?&]list=')" != "" ]; then
        output_template="${BASE_DOWNLOAD_DIR}/%(playlist)s/%(title)s-%(upload_date)s-%(id)s.%(ext)s'"
    else
        output_template="${BASE_DOWNLOAD_DIR}/%(title)s-%(upload_date)s-%(id)s.%(ext)s'"
    fi
    youtube-dl \
        -o "$output_template" \
        -x \
        --restrict-filenames \
        --audio-format mp3 \
        --ignore-errors \
        "$var" || true # True to avoid ending script when youtube-dl skip download and ignore errors.
done

deactivate

echo "$SCRIPT_NAME: Download completed."

